const assert = require('assert');
const rollup = require('rollup');
const istanbulPlugin = require( '..' );

process.chdir( __dirname );

describe('Istanbul unit test coverage plugin for Rollup', function () {
  this.timeout(15000);

  it('should transform code through istanbul instrumenter', function () {
    return rollup.rollup({
      entry: 'fixtures/main.js',
      plugins: [ istanbulPlugin() ],
      external: ['whatever']
    }).then( function ( bundle ) {
      const generated = bundle.generate();
      const code = generated.code;
      assert.ok(code.indexOf('__cov_') !== -1, code);
    });
  });

  it('should add the file name properly', function () {
    return rollup.rollup({
      entry: 'fixtures/main.js',
      plugins: [ istanbulPlugin() ],
      external: ['whatever']
    }).then( function ( bundle ) {
      const generated = bundle.generate();
      const code = generated.code;
      assert.ok(code.indexOf('fixtures/main.js') !== -1, code);
    });
  });
});
