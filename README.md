# Istanbul unit test coverage plugin for Rollup

>  A Rollup plugin to offer seamless integration with code coverage tools

This is an general coverage report plugin that aim to be up to date all the time with newest NPM packages, and provide
stability. Istanbul is used as the coverage engine.

## Install

```
npm install --save-dev gulp-istanbul
```


## How to use

In your `Karma` config file or `Rollup` config file:

```
import { rollup } from 'rollup';
import coverage from 'rollup-plugin-coverage';

rollup({
  entry: 'main.js',
  plugins: [
    // Covering files
    coveragel({
      exclude: ['test/**/*.js']
    })
  ]
}).then(...)

```

## Options

All options are optional.

### include 

Can be a minimatch pattern or an array of minimatch patterns. If is omitted or of zero length, files should be included by default; otherwise they should only be included if the ID matches one of the patterns.

### exclude

Can be a minimatch pattern or an array of minimatch patterns. Files to exclude, commonly the test files.

### coverageOptions

An object of options that will be passed to the instrumenter.

Default value:
```
{
  esModules: true,
  coverageOptions: {
    sourceMapID: id,
    sourceMapWithCode: true
  }
}
```

Istanbul is used, so check out the [readme](http://gotwarlost.github.io/istanbul/public/apidocs/classes/Instrumenter.html#method_Instrumenter) for info about other options. 
  
## Browser testing
  
For browser testing, you'll need to write the files covered by istanbul in a directory from where you'll serve these files to the browser running the test. You'll also need  way to extract the value of the coverage variable after the test have runned in the browser.

Browser testing is hard. If you're not sure what to do, then I suggest you take a look at `Karma` test runner - it has built-in coverage using `Istanbul`.

## SourceMaps

rollup-plugin-coverage supports gulp-sourcemaps when instrumenting. `Sourcemap` support can be activated / disabled with `sourceMapWithCode`. Set the value to either true or false.
  
## Known issues and workaround

There can be issues with other Rollup plugins. In most cases this can be fixed if you place this plugin *before* the other plugins.