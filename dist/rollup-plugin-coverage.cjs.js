'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var rollupPluginutils = require('rollup-pluginutils');
var istanbul = _interopDefault(require('istanbul'));

function index( options ) {
	if ( options === void 0 ) options = {};

	var filter = rollupPluginutils.createFilter(options.include, options.exclude);

	return {
		transform: function transform ( code, id ) {
		if (!filter(id)) return;

		var sourceMap  = options.sourceMapID !== false;
		var opts = Object.assign({}, options.instrumenterConfig);

		if (sourceMap ) {
			opts.codeGenerationOptions = Object.assign({},
				opts.codeGenerationOptions || {format: {compact: !opts.noCompact}},
				{sourceMap: id, sourceMapWithCode: true, file: code}
			);
		}

		opts.esModules = true;
		var instrumenter = new (options.instrumenter || istanbul).Instrumenter(opts);

		code = instrumenter.instrumentSync(code, id);

		var map;

		if (sourceMap ) {
			map = instrumenter.lastSourceMap().toJSON();
		} else {
			map =  {mappings: ''};
		}

		return { code: code, map: map };
	}
};
}

module.exports = index;