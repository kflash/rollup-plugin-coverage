import { createFilter } from 'rollup-pluginutils';
import istanbul from 'istanbul';

export default function( options = {} ) {
	const filter = createFilter(options.include, options.exclude);

	return {
	    name: 'coverage',
		transform ( code, id ) {
		if (!filter(id)) return;

		const sourceMapID = options.sourceMapID !== false;
		const opts = Object.assign({}, options.instrumenterConfig);

		if (sourceMapID) {
			opts.codeGenerationOptions = Object.assign({},
				opts.codeGenerationOptions || {format: {compact: !opts.noCompact}},
				{sourceMap: id, sourceMapWithCode: true}
			);
		}

		opts.esModules = true;
		const instrumenter = new (options.instrumenter || istanbul).Instrumenter(opts);

		code = instrumenter.instrumentSync(code, id);
		const map = sourceMapID ? instrumenter.lastSourceMap().toJSON() : {mappings: ''};

		return { code, map };
	}
};
}