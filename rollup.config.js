import buble from 'rollup-plugin-buble'

export default {
	entry: 'src/index.js',
	plugins: [buble()],
	external: ['istanbul', 'rollup-pluginutils'],
	targets: [
		{
			format: 'cjs',
			dest: 'dist/rollup-plugin-coverage.cjs.js'
		},
		{
			format: 'es',
			dest: 'dist/rollup-plugin-coverage.es.js'
		}
	]
}
